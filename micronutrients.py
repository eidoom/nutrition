#!/usr/bin/env python3

import sqlite3, collections, time, json, argparse, pathlib, itertools

import numpy
import matplotlib.pyplot, matplotlib.cm

from radar import radar_factory

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("json", type=pathlib.Path)
    parser.add_argument("type", type=str, choices=("vitamins", "minerals"))
    parser.add_argument("--title", action="store_true")
    args = parser.parse_args()

    kcals = 400

    key = args.type.title()
    with open("micronutrients.json", "r") as f:
        rawj = json.load(f)
        micros = rawj["values"][key]
        names = rawj["names"][key]

    con = sqlite3.connect("nutrition.db")
    con.row_factory = sqlite3.Row
    cur = con.cursor()

    with open(args.json, "r") as f:
        foods = json.load(f)

    start = time.time()
    res = cur.execute(
        f"""
        SELECT
          f.description,
          n.name AS nutrient,
          fn.quantity
        FROM food AS f
        INNER JOIN food_nutrient AS fn ON fn.food_id = f.id
        INNER JOIN nutrient AS n ON n.id = fn.nutrient_id
        INNER JOIN unit AS u ON u.id = n.unit_id
        WHERE
          f.description IN (
            '{"','".join([v.replace("'", "''") for v in foods.values()])}'
          )
          AND (
            n.name IN (
              '{"','".join(micros.keys())}'
            )
            OR (n.name = 'Energy' AND u.name = 'KCAL')
          )
        """
    ).fetchall()
    print(f"Query time: {int(1000 * (time.time() - start))}ms")

    raw = collections.defaultdict(lambda: collections.defaultdict(list))
    revb = {v: k for k, v in foods.items()}
    for desc, nutr, quan in res:
        raw[revb[desc]][nutr] = quan

    data = {
        food: {
            k: kcals * ns[k] / ns["Energy"] / v if k in ns else 0
            for k, v in micros.items()
        }
        for food, ns in raw.items()
    }

    # # feature vectors
    # l = numpy.array([[*item.values()] for item in [*data.values()][:4]])

    # # mean-squared-difference matrix
    # c = numpy.array(
    #     [
    #         [numpy.mean((l[i] - l[j]) ** 2) if j < i else 0 for i in range(len(l))]
    #         for j in range(len(l))
    #     ]
    # )

    # print(c)

    # # split 6 items into 2 groups such that there is highest similarity in each group

    # a = [*itertools.combinations(range(6), 3)]
    # # print(a)
    # b = [(a[i], a[-(i + 1)]) for i in range(len(a)//2)]
    # print(b)

    # exit()

    n = len(foods)
    m = 1 + n // 10

    batches = [
        collections.OrderedDict(b)
        for b in itertools.batched(
            (sorted(data.items(), key=lambda x: sum(x[1].values()), reverse=True)),
            10,
        )
    ]

    fig, axs = matplotlib.pyplot.subplots(
        nrows=m,
        sharex="col",
        layout="constrained",
        figsize=(6.4, m / 2 * 4.8),
    )
    if not isinstance(axs, numpy.ndarray):
        axs = [axs]

    if args.title:
        fig.suptitle(
            f"Proportion of daily adequate {args.type} (EFSA 2019) for average adult male in {kcals} kcals",
        )

    for ax, batch in zip(axs, batches):
        ax.set_ylabel("Proportion")

        ax.axhline(1, color="black", linewidth=0.5)

        ax.set_xticks(
            range(len(names)),
            names.values(),
        )

        ax.set_prop_cycle(color=matplotlib.cm.tab10(range(len(batch))))

        labels = [name.capitalize() for name in batch.keys()]

        ax.plot(
            [[item[a] for item in batch.values()] for a in names.keys()],
            label=labels if len(labels) > 1 else labels[0],
        )

        ax.legend(
            loc="center left",
            bbox_to_anchor=(1, 0.5),
        )

    name = f"{args.json.stem}-{args.type}.pdf"
    print(f"Plotting {name}")
    fig.savefig(name, bbox_inches="tight")
