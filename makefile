FOOD=bean nut grain vegetable mushroom

EAA=$(addsuffix s-amino-acids.pdf, $(FOOD))
MACRO=$(addsuffix s-macro.pdf, $(FOOD))
VIT=$(addsuffix s-vitamins.pdf, $(FOOD))
MIN=$(addsuffix s-minerals.pdf, $(FOOD))
ED=$(addsuffix s-energy-density.pdf, $(FOOD))
GRAPH=$(EAA) $(MACRO) $(ED) $(VIT) $(MIN)

.PHONY: all clean init

all: notes.pdf

clean:
	-rm *.pdf

notes.pdf: notes.md $(GRAPH)
	pandoc --output="$@" "$<"

$(EAA): %s-amino-acids.pdf: %s.json nutrition.db radar.py amino_acids.py amino_acids.json
	pipenv run python3 amino_acids.py $<

$(MACRO): %s-macro.pdf: %s.json nutrition.db macro_energy_ratio.py macro-energy.json
	pipenv run python3 macro_energy_ratio.py $<

$(ED): %s-energy-density.pdf: %s.json nutrition.db energy_density.py
	pipenv run python3 energy_density.py $<

$(VIT): %s-vitamins.pdf: %s.json nutrition.db micronutrients.py micronutrients.json
	pipenv run python3 micronutrients.py $< vitamins

$(MIN): %s-minerals.pdf: %s.json nutrition.db micronutrients.py micronutrients.json
	pipenv run python3 micronutrients.py $< minerals

nutrition.db: raw/ schema.sql groups.json
	pipenv run python3 make_database.py

raw/:
	$(MAKE) init
	bash download-data.sh

init: Pipfile.lock
	pipenv install
