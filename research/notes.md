# Preparation of legumes

* Legumes should be soaked in water until they reach maximum weight, then drained, then cooked in fresh water, then eaten with the broth [@fabbri.crosby:review].
* Cooked beans eaten with the broth are a good source of iron and zinc [@j.m.ea:iron].
* Legumes contain antinutrients affecting mineral absorption: phytic acid, lectins and oxalates. Soaking in water reduces lectins and oxalates. Cooking decreases all [@shi.arntfield.ea:changes].
* Cooking beans generally increases some of the micronutrient mineral concentrations across species [@ferreira.naozuka.ea:effects].
* Cooking beans without pre-soaking or discarding broth is recommended for retaining antioxidants [@ranilla.genovese.ea:effect].
* Cooking beans reduces antinutrient concentrations (phytic acid and tannins) [@rehman.shah:thermal].
* Soaking water should be discarded before cooking [@fernandes.nishida.ea:influence].
* Cooking soaked chickpeas improves nutrition, although there are losses in vitamins and minerals: boiling (90 minutes) gave significant losses, while microwaving (15 minutes) had slight losses and is recommended [@el-adawy:nutritional].
* For chickpeas, discard soaking water to reduce tannins [@idate.shah.ea:comprehensive].

# Vegan diet

* Vegan athletes must take care to get enough energy, protein, fat, vitamin B12, *n*-3 fats, calcium, iodine, iron and zinc [@rogerson:vegan].
* Vegans should take care to get enough vitamin B12, zinc, calcium, and selenium [@bakaloudi.halloran.ea:intake].

# Huel

* "[C]onsuming only Huel for 4 weeks does not negatively affect micronutrient status" [@wilcox.chater.ea:pilot].
