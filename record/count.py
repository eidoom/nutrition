#!/usr/bin/env python3

import argparse, pathlib, json


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("file", type=pathlib.Path)
    args = parser.parse_args()

    with open(args.file, "r") as f:
        data = json.load(f)

    totals = {
        x: sum(
            item["nutrients"][x]
            * item["quantity"]
            / 100
            * (item["count"] if "count" in item else 1)
            for item in data.values()
        )
        for x in ("energy", "protein")
    }

    print(totals)
