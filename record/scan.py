#!/usr/bin/env python3

import json, atexit
import imageio, pyzbar.pyzbar, openfoodfacts


def farewell(output_data, filename):
    print("Writing data to file")

    with open(filename, "w") as f:
        json.dump(output_data, f)


if __name__ == "__main__":
    nutrient_labels = {
        "carbohydrate": "carbohydrates_100g",
        "energy": "energy-kcal_100g",
        "fat": "fat_100g",
        "fibre": "fiber_100g",
        "protein": "proteins_100g",
        "salt": "salt_100g",
        "saturated fat": "saturated-fat_100g",
        "sugar": "sugars_100g",
    }

    outputs = {}

    atexit.register(farewell, outputs, "output.json")

    print("Scanning...")

    for frame in imageio.v3.imiter("<video0>"):
        results = pyzbar.pyzbar.decode(frame)

        if results:
            print("\a", end="")

            for result in results:
                barcode = result.data.decode()

                if barcode in outputs:
                    print("Duplicate")
                    continue

                print(barcode)

                try:
                    data = openfoodfacts.products.get_product(barcode)["product"]
                except KeyError:
                    print("No product found")
                    continue

                outputs[barcode] = {
                    "name": data["product_name"],
                    "nutrients": {
                        my_name: float(data["nutriments"][off_name])
                        for my_name, off_name in nutrient_labels.items()
                        if off_name in data["nutriments"]
                    },
                }

                try:
                    outputs[barcode]["brand"] = data["brands"]
                except KeyError:
                    pass

                try:
                    outputs[barcode]["quantity"] = float(data["product_quantity"])
                except KeyError:
                    pass
