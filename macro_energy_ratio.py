#!/usr/bin/env python3

import sqlite3, collections, time, json, argparse, pathlib

import numpy, matplotlib.pyplot, matplotlib.cm, mpltern

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("json", type=pathlib.Path)
    parser.add_argument("--title", action="store_true")
    args = parser.parse_args()

    con = sqlite3.connect("nutrition.db")
    cur = con.cursor()

    nutrients = {
        "energy": "Energy",
        "carbohydrate": "Carbohydrate, by difference",
        "protein": "Protein",
        "fat": "Total lipid (fat)",
    }

    with open(args.json, "r") as f:
        foods = json.load(f)

    start = time.time()
    res = cur.execute(
        f"""
        SELECT
          f.description,
          n.name AS nutrient,
          fn.quantity
        FROM food AS f
        INNER JOIN food_nutrient AS fn ON fn.food_id = f.id
        INNER JOIN nutrient AS n ON n.id = fn.nutrient_id
        INNER JOIN unit AS u ON u.id = n.unit_id
        WHERE
          f.description IN (
            '{"','".join([v.replace("'", "''") for v in foods.values()])}'
          )
          AND n.name IN (
            '{"','".join(nutrients.values())}'
          )
          AND u.name IN (
            'G',
            'KCAL'
          )
        """
    ).fetchall()
    print(f"Query time: {int(1000 * (time.time() - start))}ms")

    data = collections.defaultdict(lambda: collections.defaultdict(list))
    revb = {v: k for k, v in foods.items()}
    revn = {v: k for k, v in nutrients.items()}
    for desc, nutr, quan in res:
        data[revb[desc]][revn[nutr]] = quan
    # data = {k: {**v} for k, v in data.items()}

    # for food, datum in data.items():
    #     data[food]["fat"] = datum["fat"] * 9 / datum["energy"]
    #     data[food]["carbohydrate"] = datum["carbohydrate"] * 4 / datum["energy"]
    #     data[food]["protein"] = datum["protein"] * 4 / datum["energy"]

    with open("macro-energy.json", "r") as f:
        me = json.load(f)

    for food, datum in data.items():
        for m, e in me.items():
            data[food][m] *= e / datum["energy"]

        # print(
        #     sum(data[food][x] for x in ("fat", "protein", "carbohydrate"))
        #     - data[food]["energy"],
        # )

    # for food, prof in data.items():
    #     print(food)
    #     print(prof)

    colours = matplotlib.cm.tab20(range(20))
    colours = numpy.concatenate((colours[::2], colours[1::2]))

    fig, ax = matplotlib.pyplot.subplots(subplot_kw=dict(projection="ternary"), layout="constrained")

    if args.title:
        ax.set_title("Macronutrient energy split", position=(0.7, 1))

    ax.grid()

    for (food, prof), colour in zip(
        sorted(data.items(), key=lambda x: x[1]["protein"], reverse=True), colours
    ):
        ax.scatter(
            prof["protein"],
            prof["carbohydrate"],
            prof["fat"],
            s=72,
            label=food.capitalize(),
            color=colour,
        )

    ax.set_tlabel("Protein")
    ax.set_llabel("Carbohydrate")
    ax.set_rlabel("Fat")

    ax.legend(
        loc="center left",
        bbox_to_anchor=(1.15, 0.5),
    )

    fig_name = f"{args.json.stem}-macro.pdf"
    print(f"Plotting {fig_name}")
    fig.savefig(fig_name, bbox_inches="tight")
