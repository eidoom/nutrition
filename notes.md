# Nutritional profiles

The following is for raw foods.

## Legumes

![Macronutrient energy split](beans-macro.pdf)

![Proportion of WHO RDA (2007) of essential amino acids (EEAs) for 70 kg person in 400 kcals](beans-amino-acids.pdf)

![Energy densities](beans-energy-density.pdf)

![Proportion of daily adequate vitamins (EFSA 2019) for average adult male in 400 kcals](beans-vitamins.pdf)

![Proportion of daily adequate minerals (EFSA 2019) for average adult male in 400 kcals](beans-minerals.pdf)

\clearpage

## Nuts and seeds

![Macronutrient energy split](nuts-macro.pdf)

![Proportion of WHO RDA (2007) of essential amino acids (EEAs) for 70 kg person in 400 kcals](nuts-amino-acids.pdf)

![Energy densities](nuts-energy-density.pdf)

![Proportion of daily adequate vitamins (EFSA 2019) for average adult male in 400 kcals](nuts-vitamins.pdf)

![Proportion of daily adequate minerals (EFSA 2019) for average adult male in 400 kcals](nuts-minerals.pdf)

\clearpage

## Grains

![Macronutrient energy split](grains-macro.pdf)

![Proportion of WHO RDA (2007) of essential amino acids (EEAs) for 70 kg person in 400 kcals](grains-amino-acids.pdf)

![Energy densities](grains-energy-density.pdf)

![Proportion of daily adequate vitamins (EFSA 2019) for average adult male in 400 kcals](grains-vitamins.pdf)

![Proportion of daily adequate minerals (EFSA 2019) for average adult male in 400 kcals](grains-minerals.pdf)

\clearpage

## Vegetables

![Macronutrient energy split](vegetables-macro.pdf)

![Proportion of WHO RDA (2007) of essential amino acids (EEAs) for 70 kg person in 400 kcals](vegetables-amino-acids.pdf)

![Energy densities](vegetables-energy-density.pdf)

![Proportion of daily adequate vitamins (EFSA 2019) for average adult male in 400 kcals](vegetables-vitamins.pdf){ height=95% }

![Proportion of daily adequate minerals (EFSA 2019) for average adult male in 400 kcals](vegetables-minerals.pdf){ height=95% }

\clearpage

## Mushrooms

![Macronutrient energy split](mushrooms-macro.pdf)

![Proportion of WHO RDA (2007) of essential amino acids (EEAs) for 70 kg person in 400 kcals](mushrooms-amino-acids.pdf)

![Energy densities](mushrooms-energy-density.pdf)

![Proportion of daily adequate vitamins (EFSA 2019) for average adult male in 400 kcals](mushrooms-vitamins.pdf)

![Proportion of daily adequate minerals (EFSA 2019) for average adult male in 400 kcals](mushrooms-minerals.pdf)
