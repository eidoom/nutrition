#!/usr/bin/env python3

import csv, pathlib, sqlite3, json, argparse, time

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--force", action="store_true")
    args = parser.parse_args()

    db = pathlib.Path("nutrition.db")

    if args.force:
        db.unlink(missing_ok=True)

    elif db.is_file():
        print(f"{db} already exists")
        exit()

    start = time.time()

    con = sqlite3.connect(db)
    cur = con.cursor()

    with open("schema.sql", "r") as f:
        cur.executescript(f.read())

    root = pathlib.Path("raw") / "csv"

    with open(root / "supporting" / "food_category.csv", "r", newline="") as f:
        cur.executemany(
            """
            INSERT INTO food_category (id, description)
            VALUES (:id, :description)
            """,
            csv.DictReader(f),
        )

    with open(root / "legacy" / "food.csv", "r", newline="") as f:
        cur.executemany(
            """
            INSERT INTO food (id, description, food_category_id)
            VALUES (:fdc_id, :description, :food_category_id)
            """,
            csv.DictReader(f),
        )

    with open(root / "supporting" / "nutrient.csv", "r", newline="") as f:
        for r in csv.DictReader(f):
            res = cur.execute("SELECT id FROM unit WHERE name = ?", (r["unit_name"],))
            fet = res.fetchone()
            if fet is None:
                cur.execute("INSERT INTO unit (name) VALUES (?)", (r["unit_name"],))
                uid = cur.lastrowid
            else:
                uid = fet[0]

            cur.execute(
                """
                INSERT INTO nutrient (id, name, unit_id)
                VALUES (?, ?, ?)
                """,
                (r["id"], r["name"], uid),
            )

    with open(root / "legacy" / "food_nutrient.csv", "r", newline="") as f:
        cur.executemany(
            """
            INSERT INTO food_nutrient (id, food_id, nutrient_id, quantity)
            VALUES (:id, :fdc_id, :nutrient_id, :amount)
            """,
            csv.DictReader(f),
        )

    with open("groups.json", "r") as f:
        groups = json.load(f)

    for group, nutrients in groups.items():
        cur.execute("INSERT INTO nutrient_group (name) VALUES (?)", (group,))
        ngid = cur.lastrowid

        ns = "','".join(nutrients)
        nids = cur.execute(
            f"SELECT id FROM nutrient WHERE name IN ('{ns}')",
        ).fetchall()

        cur.executemany(
            "INSERT INTO nutrient_group_member (nutrient_id, nutrient_group_id) VALUES (?, ?)",
            [(nid[0], ngid) for nid in nids],
        )

    con.commit()
    con.close()

    print(f"Took {1000 * (time.time() - start):.0f}ms")
