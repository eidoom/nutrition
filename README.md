# [nutrition](https://gitlab.com/eidoom/nutrition)

[FoodData Central](https://fdc.nal.usda.gov/) by U.S. Department of Agriculture seems good data source.
Data in public domain.
Download or use API.

Usage (requires `pipenv`):
```shell
make -j
xdg-open notes.pdf
```
