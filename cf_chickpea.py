#!/usr/bin/env python3

import pathlib, sqlite3, time, collections, json

import matplotlib.pyplot, mpltern

if __name__ == "__main__":
    db = pathlib.Path("nutrition.db")

    con = sqlite3.connect(db)
    cur = con.cursor()

    nutrients = {
        "Energy": "energy",
        "Carbohydrate, by difference": "carbohydrate",
        "Protein": "protein",
        "Total lipid (fat)": "fat",
    }

    start = time.time()
    res = cur.execute(
        f"""
        SELECT
          f.description,
          n.name AS nutrient,
          fn.quantity
        FROM food AS f
        INNER JOIN food_nutrient AS fn ON fn.food_id = f.id
        INNER JOIN nutrient AS n ON n.id = fn.nutrient_id
        INNER JOIN unit AS u ON u.id = n.unit_id
        WHERE
          f.description LIKE 'chickpea%'
          AND n.name IN (
            '{"','".join(nutrients.keys())}'
          )
          AND u.name IN (
            'G',
            'KCAL'
          )
        """
    ).fetchall()
    print(f"Query time: {int(1000 * (time.time() - start))}ms")

    data = collections.defaultdict(lambda: collections.defaultdict(list))
    for (desc, nutr, quan) in res:
        data[desc][nutrients[nutr]] = quan

    with open("macro-energy.json", "r") as f:
        me = json.load(f)

    for food, datum in data.items():
        for m, e in me.items():
            data[food][m] *= e / datum["energy"]

    fig, ax = matplotlib.pyplot.subplots(subplot_kw=dict(projection="ternary"))

    ax.set_title("Macronutrient energy ratio")

    ax.grid()

    for food, prof in sorted(data.items(), key=lambda x: x[1]["protein"], reverse=True):
        ax.scatter(
            prof["protein"],
            prof["carbohydrate"],
            prof["fat"],
            s=72,
            label=food,
        )

    ax.set_tlabel("Protein")
    ax.set_llabel("Carbohydrate")
    ax.set_rlabel("Fat")

    ax.legend(
        loc="upper center",
        bbox_to_anchor=(0.5, -0.2),
    )

    fig_name = f"cf-chickpea.pdf"
    print(f"Plotting {fig_name}")
    fig.savefig(fig_name, bbox_inches="tight")
