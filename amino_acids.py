#!/usr/bin/env python3

import sqlite3, collections, time, json, argparse, pathlib

import matplotlib.pyplot

from radar import radar_factory

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("json", type=pathlib.Path)
    parser.add_argument("--title", action="store_true")
    args = parser.parse_args()

    mass = 70  # kg
    kcals = 400

    with open("amino_acids.json", "r") as f:
        who_rda = json.load(f)["values"]
    who_rda_abs = {x["name"]: x["quantity"] * mass / 1000 for x in who_rda}  # g

    con = sqlite3.connect("nutrition.db")
    con.row_factory = sqlite3.Row
    cur = con.cursor()

    with open("groups.json", "r") as f:
        eaas = json.load(f)["Essential amino acids"]

    nutrients = ["Energy", *eaas]

    with open(args.json, "r") as f:
        foods = json.load(f)

    start = time.time()
    res = cur.execute(
        f"""
        SELECT
          f.description,
          n.name AS nutrient,
          fn.quantity
        FROM food AS f
        INNER JOIN food_nutrient AS fn ON fn.food_id = f.id
        INNER JOIN nutrient AS n ON n.id = fn.nutrient_id
        INNER JOIN unit AS u ON u.id = n.unit_id
        WHERE
          f.description IN (
            '{"','".join([v.replace("'", "''") for v in foods.values()])}'
          )
          AND n.name IN (
            '{"','".join(nutrients)}'
          )
          AND u.name IN (
            'G',
            'KCAL'
          )
        """
    ).fetchall()
    print(f"Query time: {int(1000 * (time.time() - start))}ms")

    raw = collections.defaultdict(lambda: collections.defaultdict(list))
    revb = {v: k for k, v in foods.items()}
    for (desc, nutr, quan) in res:
        raw[revb[desc]][nutr] = quan

    data = {
        food: {
            k: round(kcals * ns[k] / ns["Energy"] / v, 1)
            for k, v in who_rda_abs.items()
        }
        for food, ns in raw.items()
        if all(k in ns for k in who_rda_abs.keys())
    }

    spoke_labels = sorted(eaas)

    theta = radar_factory(len(spoke_labels))

    fig, ax = matplotlib.pyplot.subplots(subplot_kw=dict(projection="radar"))

    # ax.set_rgrids([0.25, 0.5, 1, 2, 4])
    # ax.set_rlim([0, 4])
    if args.title:
        ax.set_title(
            f"Proportion of WHO RDA (2007) of EAAs for {mass} kg person in {kcals} kcals",
            position=(0.75, 1),
        )

    ordered = sorted(
        data.items(), key=lambda x: min(x[1][a] for a in spoke_labels), reverse=True
    )

    n = 3
    for label, datum in ordered[:n] + ordered[-n:]:
        ax.plot(theta, [datum[k] for k in spoke_labels], label=label.capitalize())

    ax.set_varlabels(spoke_labels)

    xx = 1.15
    handles, _ = ax.get_legend_handles_labels()
    leg1 = ax.legend(
        handles=handles[:n],
        title=f"Top {n} by min EAA",
        loc="center left",
        bbox_to_anchor=(xx, 0.75),
    )
    ax.add_artist(leg1)
    leg2 = ax.legend(
        handles=handles[n:],
        title=f"Bottom {n} by min EAA",
        loc="center left",
        bbox_to_anchor=(xx, 0.25),
    )

    name = f"{args.json.stem}-amino-acids.pdf"
    print(f"Plotting {name}")
    fig.savefig(name, bbox_inches="tight", bbox_extra_artists=(leg1, leg2))
