#!/usr/bin/env python3

import sqlite3, collections, time, json, argparse, pathlib

import numpy, matplotlib.pyplot, matplotlib.cm

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("json", type=pathlib.Path)
    parser.add_argument("--title", action="store_true")
    args = parser.parse_args()

    con = sqlite3.connect("nutrition.db")
    cur = con.cursor()

    with open(args.json, "r") as f:
        foods = json.load(f)

    start = time.time()
    res = cur.execute(
        f"""
        SELECT
          f.description,
          4e4 / fn.quantity AS mass -- g per 400 kcals
        FROM food AS f
        INNER JOIN food_nutrient AS fn ON fn.food_id = f.id
        INNER JOIN nutrient AS n ON n.id = fn.nutrient_id
        INNER JOIN unit AS u ON u.id = n.unit_id
        WHERE
          f.description IN (
            '{"','".join([v.replace("'", "''") for v in foods.values()])}'
          )
          AND n.name = 'Energy'
          AND u.name = 'KCAL'
        ORDER BY mass DESC
        """
    ).fetchall()
    print(f"Query time: {int(1000 * (time.time() - start))}ms")

    rev = {v: k for k, v in foods.items()}

    labels = [rev[r[0]].capitalize() for r in res]
    vals = [r[1] for r in res]

    colours = matplotlib.cm.tab20(range(20))
    colours = numpy.concatenate((colours[::2], colours[1::2]))

    fig, ax = matplotlib.pyplot.subplots()

    if args.title:
        ax.set_title("Energy density")

    ax.set_xlabel("Mass of 400 kcals (g)")

    for n, v, c in zip(labels, vals, colours[0 : len(labels)][::-1]):
        ax.barh(
            y=n,
            width=v,
            color=c,
        )

    fig_name = f"{args.json.stem}-energy-density.pdf"
    print(f"Plotting {fig_name}")
    fig.savefig(fig_name, bbox_inches="tight")
