#!/usr/bin/env bash

mkdir -p raw/csv/legacy
cd raw/csv/legacy
wget https://fdc.nal.usda.gov/fdc-datasets/FoodData_Central_sr_legacy_food_csv_%202019-04-02.zip
unzip 'FoodData_Central_sr_legacy_food_csv_ 2019-04-02.zip'
cd ..
mkdir supporting
cd supporting
wget https://fdc.nal.usda.gov/fdc-datasets/FoodData_Central_Supporting_Data_csv_2022-10-28.zip
unzip FoodData_Central_Supporting_Data_csv_2022-10-28.zip
cd ../../..
