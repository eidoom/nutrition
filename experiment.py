#!/usr/bin/env python3

import csv, pathlib, sqlite3, json

if __name__ == "__main__":
    db = pathlib.Path("nutrition.db")
    con = sqlite3.connect(db)
    cur = con.cursor()

    # fs = cur.execute(
    #     """
    #     SELECT
    #       description
    #     FROM food_category
    #     ORDER BY description
    #     """
    # ).fetchall()
    # for (f,) in fs:
    #     print(f)
    # print()
    # exit()

    # fs = cur.execute(
    #     """
    #     SELECT
    #       f.description
    #     FROM food AS f
    #     INNER JOIN food_category AS c ON c.id = f.food_category_id
    #     WHERE
    #       f.description LIKE '%raw%'
    #       AND
    #       c.description = 'Vegetables and Vegetable Products'
    #     ORDER BY f.description
    #     """
    # ).fetchall()
    # for (f,) in fs:
    #     print(f)
    # print()
    # exit()

    # food = "Chickpeas (garbanzo beans, bengal gram), mature seeds, cooked, boiled, with salt"
    # print(food)

    # res = cur.execute(
    #     """
    #     SELECT
    #       n.name,
    #       fn.quantity,
    #       u.name
    #     FROM food AS f
    #     INNER JOIN food_nutrient AS fn ON fn.food_id = f.id
    #     INNER JOIN nutrient AS n ON n.id = fn.nutrient_id
    #     INNER JOIN unit AS u ON u.id = n.unit_id
    #     INNER JOIN nutrient_group_member AS m ON m.nutrient_id = n.id
    #     INNER JOIN nutrient_group AS g ON g.id = m.nutrient_group_id
    #     WHERE
    #       f.description = ?
    #       AND g.name = ?
    #     """,
    #     (food, "Macronutrients"),
    # )
    # for r in res.fetchall():
    #     print(r)


    # food = "Chickpeas (garbanzo beans, bengal gram), mature seeds, cooked, boiled, with salt"
    # res = cur.execute(
    #     """
    #     SELECT
    #       n.name,
    #       fn.quantity,
    #       u.name
    #     FROM food AS f
    #     INNER JOIN food_nutrient AS fn ON fn.food_id = f.id
    #     INNER JOIN nutrient AS n ON n.id = fn.nutrient_id
    #     INNER JOIN unit AS u ON u.id = n.unit_id
    #     WHERE
    #       f.description = ?
    #     ORDER BY n.name
    #     """,
    #     (food,),
    # )
    # for r in res.fetchall():
    #     print(r)

    # res = cur.execute(
    #     """
    #     SELECT
    #       f.description,
    #       fn1.quantity,
    #       fn2.quantity
    #     FROM food AS f
    #     INNER JOIN food_nutrient AS fn1 ON fn1.food_id = f.id
    #     INNER JOIN nutrient AS n1 ON n1.id = fn1.nutrient_id
    #     INNER JOIN food_nutrient AS fn2 ON fn2.food_id = f.id
    #     INNER JOIN nutrient AS n2 ON n2.id = fn2.nutrient_id
    #     WHERE
    #       n1.name LIKE 'Folate, DFE'
    #       AND
    #       n2.name LIKE 'Folate, total'
    #       AND
    #       fn2.quantity > 0
    #     """,
    #     # (food,),
    # )
    # for r in res.fetchall():
    #     print(r)

    # res = cur.execute(
    #     """
    #     SELECT
    #       n.name,
    #       u.name
    #     FROM nutrient AS n
    #     INNER JOIN unit AS u ON u.id = n.unit_id
    #     INNER JOIN nutrient_group_member AS m ON m.nutrient_id = n.id
    #     INNER JOIN nutrient_group AS g ON g.id = m.nutrient_group_id
    #     WHERE
    #       g.name = ?
    #     ORDER BY n.name
    #     """,
    #     ("Vitamins",),
    # )
    # for r in res.fetchall():
    #     print(r)

    res = cur.execute(
        """
        SELECT
          n.name
        FROM nutrient AS n
        WHERE
          n.name LIKE '%biotin%'
        ORDER BY n.name
        """,
    )
    for r in res.fetchall():
        print(r)

    # res = cur.execute(
    #     """
    #     SELECT
    #       f.description,
    #       n.name,
    #       fn.quantity,
    #       u.name
    #     FROM food AS f
    #     INNER JOIN food_nutrient AS fn ON fn.food_id = f.id
    #     INNER JOIN nutrient AS n ON n.id = fn.nutrient_id
    #     INNER JOIN unit AS u ON u.id = n.unit_id
    #     WHERE
    #       n.name = 'Sodium, Na'
    #       AND fn.quantity > 0
    #     """,
    # )
    # for r in res.fetchall():
    #     print(r)
